//
//  Relation.h
//  ManagedObjectKeyValueParser
//
//  Created by Richard Moult on 11/12/2013.
//  Copyright (c) 2013 trickysquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event;

@interface Relation : NSManagedObject

@property (nonatomic, retain) NSString * dummy;
@property (nonatomic, retain) Event *event;

@end
