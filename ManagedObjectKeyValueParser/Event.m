//
//  Event.m
//  ManagedObjectKeyValueParser
//
//  Created by Richard Moult on 11/12/2013.
//  Copyright (c) 2013 trickysquirrel. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic flag;
@dynamic name;
@dynamic timeStamp;
@dynamic value;
@dynamic relationship;

@end
