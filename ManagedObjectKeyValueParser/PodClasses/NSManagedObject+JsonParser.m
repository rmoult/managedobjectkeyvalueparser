//
//  Created by Richard Moult on 12/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import "NSManagedObject+JsonParser.h"




NSString * ManageObjectParserDomainError = @"ManageObjectParserDomainError";
NSString * ManagedObjectErrorAttributeNameKey = @"ManagedObjectErrorAttributeNameKey";



@implementation NSManagedObject (JsonParser)




- (BOOL)populateFromDictionary:(NSDictionary *)dictionary error:(NSError **)error
{
    if (!dictionary || ![dictionary isKindOfClass:[NSDictionary class]])
    {
        if (error != NULL)
        {
            *error = [self errorForIncorrectParameterWithMessage:@"The operation populateFromDictionary could not be completed due to an invalid dictionary"];
        }
        return NO;
    }
    
    NSEntityDescription * entity = [self entityForClass];
    
    NSArray * entityProperties = entity.properties;
    
    for (NSAttributeDescription * attributeDescription in entityProperties)
    {
        if ([self isReleationshipAttribute:attributeDescription])
        {
            continue;
        }
        
        [self parseDictionary:dictionary forAttributeDescription:attributeDescription];
    }
    
    BOOL success = [self isValidEntityWithError:error];

    return success;
}



- (NSError *)errorForIncorrectParameterWithMessage:(NSString *)msg
{
    NSMutableDictionary * userInfoDict = nil;
    
    if (msg)
    {
        userInfoDict = [@{ NSLocalizedDescriptionKey: msg } mutableCopy];
    }
    
    return [NSError errorWithDomain:ManageObjectParserDomainError code:ManageObjectParserErrorInValidParameter userInfo:userInfoDict];
}



- (NSEntityDescription *)entityForClass
{
    NSArray * entitiesInModel = self.managedObjectContext.persistentStoreCoordinator.managedObjectModel.entities;
    
    for (NSEntityDescription * entity in entitiesInModel)
    {
        if ([[entity managedObjectClassName] isEqualToString: NSStringFromClass([self class])])
        {
            return entity;
        }
    }
    return nil;
}




- (BOOL)isReleationshipAttribute:(NSAttributeDescription *)attribute
{
    if ([attribute isMemberOfClass:[NSAttributeDescription class]] || !attribute)
    {
        return NO;
    }
    
    return YES;
}



- (void)parseDictionary:(NSDictionary *)dictionary forAttributeDescription:(NSAttributeDescription *)attributeDescription
{
    Class expectedClass = NSClassFromString([attributeDescription attributeValueClassName]);
    
    NSArray * userInfoKeys = [attributeDescription.userInfo allKeys];
    
    for (NSString * key in userInfoKeys)
    {
        NSString * attributeKeyValue = attributeDescription.userInfo[key];
        
        id value = [dictionary objectForKey:attributeKeyValue];
        
        [self setValue:value forExpectedClass:expectedClass forKey:attributeDescription.name];
    }
}




- (void)setValue:(id)value forExpectedClass:(Class)expectedClass forKey:(NSString *)key
{
    if ([self isValidValue:value])
    {
        if ([value isKindOfClass:[NSString class]])
        {
            id tempValue = [self convertToNumberBoolFromString:value];
            
            if (tempValue)
            {
                value = tempValue;
            }
        }
        
        if ([value isKindOfClass:expectedClass]) {
            [self setValue:value forKey:key];
        }
        else {
            NSLog(@"unexpected class type %@  %@", [value class], expectedClass);
        }
        
    }
}




- (NSNumber *)convertToNumberBoolFromString:(NSString *)string
{
    if ([string isEqualToString:@"true"])
    {
        return @YES;
    }
    else if ([string isEqualToString: @"false"])
    {
        return @NO;
    }
    return nil;
}




- (BOOL)isValidValue:(id)value
{
    if ( ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]] || value == (id)[NSNull class]) )
    {
        return NO;
    }
    else if (value)
    {
        return YES;
    }
    return NO;
}



- (BOOL)isValidEntityWithError:(NSError **)error
{
    NSEntityDescription * entityDescription = [self entityForClass];
    
    for (NSAttributeDescription * attributeDescription in entityDescription.properties)
    {
        if ([self isReleationshipAttribute:attributeDescription])
        {
            continue;
        }
        
        if (!attributeDescription.isOptional && ![self valueForKey:attributeDescription.name])
        {
            if (error != NULL)
            {
                *error = [self errorForNonOptionalAttribute:attributeDescription];
            }
            return NO;
        }
    }
    return YES;
}



- (NSError *)errorForNonOptionalAttribute:(NSAttributeDescription *)attributeDescription
{
    NSMutableDictionary * userInfoDict = [@{ NSLocalizedDescriptionKey: @"Managed object failed due to an empty non optional attribue" } mutableCopy];
    
    if (attributeDescription)
    {
        [userInfoDict setObject:attributeDescription.name forKey:ManagedObjectErrorAttributeNameKey];
    }
    
    return [NSError errorWithDomain:ManageObjectParserDomainError code:ManageObjectParseErrorNoneOptionalAttribute userInfo:userInfoDict];
}



@end
