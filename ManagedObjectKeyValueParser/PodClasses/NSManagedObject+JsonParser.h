//
//  NSManagedObject+JsonParser.h
//  ManagedObjectKeyValueParser
//
//  Created by Richard Moult on 11/12/2013.
//  Copyright (c) 2013 trickysquirrel. All rights reserved.
//

#import <CoreData/CoreData.h>



extern NSString * ManageObjectParserDomainError;
extern NSString * ManagedObjectErrorAttributeNameKey;


typedef enum _ManageObjectParserErrorCodes : NSInteger
{
    ManageObjectParseErrorNoneOptionalAttribute = 0,
    ManageObjectParserErrorInValidParameter
} ManageObjectParserErrorCodes;




@interface NSManagedObject (JsonParser)


/*!
 Uses this managed object attributes user info values to match against dictionary keys.  Once matched the dictionary key value is then 
 assigned to the this object matching attribute info value.
 It is possible that attribute contains more that one key/value pair, this allows correct parsing when using several data sources containing
 the same information but have different key names.
 The returning error will contain the domain ManageObjectParserDomainError and at the moment the only code
 is ManageObjectParserErrorInValidParameter.  Error also contains a EN localized description mainly for the developer to understand, it is 
 recogmended that the error code is matched to a localized string.
 You can complement this function by using isValidEntityWithError to check all none optional entities have been assigned a value.
 
 @param error If the managed object fails to parse or contains a non optional attribute.  
 
 @return YES if parsing could take place
 */
- (BOOL)populateFromDictionary:(NSDictionary *)dictionary error:(NSError **)error;


/*!
 Returns YES if all non optional attributes have been set.
 This is called within the populateFromDictionary by default.
 */
- (BOOL)isValidEntityWithError:(NSError **)error;


@end
