//
//  Event.h
//  ManagedObjectKeyValueParser
//
//  Created by Richard Moult on 11/12/2013.
//  Copyright (c) 2013 trickysquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Event : NSManagedObject

@property (nonatomic, retain) NSNumber * flag;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) NSManagedObject *relationship;

@end
