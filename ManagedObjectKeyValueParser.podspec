
Pod::Spec.new do |s|

s.name         = "ManagedObjectKeyValueParser"
s.version      = "1.0.0"
s.summary      = "Add values to a managed object through entity keys matching JSON keys"
s.author       = "Richard Moult"
s.homepage     = "https://rmoult@bitbucket.org/rmoult/managedobjectkeyvalueparser"
s.license      = { :type => 'The MIT License (MIT)', :text => <<-LICENSE
Copyright (c) 2015 Richard Moult.
LICENSE
}
s.source_files = 'ManagedObjectKeyValueParser/PodClasses/*.{h,m}'
s.requires_arc = true
s.platform     = :ios, '7.0'
s.frameworks   = 'CoreData'
s.source       = {:git => "https://rmoult@bitbucket.org/rmoult/managedobjectkeyvalueparser.git", :tag=> '1.0.0'}

end
