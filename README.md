# ManagedObjectKeyValueParser
Add values to a managed object through entity keys matching JSON keys

# Pod install
```
pod 'ManagedObjectKeyValueParser', :git => 'https://rmoult@bitbucket.org/rmoult/managedobjectkeyvalueparser.git'
```

# Set up Core Data

Once a project is created with a core data and an entity with attributes has been set up.  In the data model select an entity attribute and in the right side panel in the 'User Info' section add a key.  This will create a new key value pair.  There is no need to amend the 'key' just alter the value so that it matches that of the JSON key.  

If you are developing against various backends/versions (staging, development) and they contain different keys for the same value, you can simple and in another key with a different value.

Errors : 
If the managed object fails to parse or contains a non optional attribute an error will be returned.


# Example

```
self.event = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:maangedObjectContext];
[self.event populateFromDictionary:@{@"key":@"value"} error:nil];
```

# Contributors

Taken from some work from Ashley Mills, re-written, unit tested and turned into a pod