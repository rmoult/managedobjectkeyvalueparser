//
//  ManagedObjectKeyValueParserTests.m
//  ManagedObjectKeyValueParserTests
//
//  Created by Richard Moult on 11/12/2013.
//  Copyright (c) 2013 trickysquirrel. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AppDelegate.h"
#import "Event.h"
#import "NSManagedObject+JsonParser.h"



@interface NSManagedObject (JsonParserPrivateFunctions)
- (NSEntityDescription *)entityForClass;
- (void)parseDictionary:(NSDictionary *)dictionary forAttributeDescription:(NSAttributeDescription *)attributeDescription;
- (BOOL)isValidValue:(id)value;
- (BOOL)isReleationshipAttribute:(NSAttributeDescription *)attribute;
- (NSNumber *)convertToNumberBoolFromString:(NSString *)string;
- (NSError *)errorForIncorrectParameterWithMessage:(NSString *)msg;
- (BOOL)isValidEntityWithError:(NSError **)error;
@end






@interface ManagedObjectKeyValueParserTests : XCTestCase
@property (strong, nonatomic) NSManagedObjectContext * moc;
@property (strong, nonatomic) Event * event;
@end





@implementation ManagedObjectKeyValueParserTests

- (void)setUp
{
    [super setUp];
    
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.moc = [appDelegate managedObjectContext];
    
    self.event = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:self.moc];
}



- (NSData *)dataForFileName:(NSString *)fileName ofType:(NSString *)type
{
    NSString * filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:type];

    return [NSData dataWithContentsOfFile:filePath];
}



- (NSDictionary *)dictionaryForJsonFileName:(NSString *)fileName
{
    NSData * fileData = [self dataForFileName:fileName ofType:@"json"];
    
    NSDictionary * jsonDictionary = [NSJSONSerialization JSONObjectWithData:fileData options:0 error:nil];
    
    return jsonDictionary;
}





#pragma mark - testing helper functions and set up

- (void)testDataForFileName
{
    NSData * data = [self dataForFileName:@"single" ofType:@"json"];
    
    XCTAssertNotNil(data, @"could not retreive bundle file single");
}



- (void)testDictionaryForJsonFileName
{
    NSDictionary * dictionary = [self dictionaryForJsonFileName:@"single"];
    
    XCTAssertNotNil(dictionary, @"could not convert bundle file single to dictionary");
}



- (void)testSetUp
{
    XCTAssertNotNil(self.moc, @"set up did not create the moc correctly");
    
    XCTAssertNotNil(self.event, @"set up did not create the event correctly");
}






#pragma mark - testing code


- (void)testEntityForClass
{
    NSEntityDescription * eventEntityDescription = [self.event entityForClass];
    
    XCTAssertNotNil(eventEntityDescription, @"this should return the description for the event class");
}



- (void)testNoneReleationShipAttribute
{
    NSArray * entityProperties = [self.event entityForClass].properties;
    
    for (NSAttributeDescription * attributeDescription in entityProperties)
    {
        if ([attributeDescription.name isEqualToString:@"name"])
        {
            XCTAssertFalse([self.event isReleationshipAttribute:attributeDescription], @"this should not fail as it is not a relationship");
        }
    }
}



- (void)testReleationShipAttribute
{
    NSArray * entityProperties = [self.event entityForClass].properties;
    
    for (NSAttributeDescription * attributeDescription in entityProperties)
    {
        if ([attributeDescription.name isEqualToString:@"relationship"])
        {
            XCTAssertTrue([self.event isReleationshipAttribute:attributeDescription], @"this should not fail as it is not a relationship");
        }
    }
}



- (void)testReleationShipAttributeForNil
{
    XCTAssertFalse([self.event isReleationshipAttribute:nil], @"this should not fail as it is not a relationship");
}



- (void)testIsValidValue
{
    XCTAssertTrue([self.event isValidValue:@"a"], @"a string should be valid");
    
    XCTAssertTrue([self.event isValidValue:@1], @"a number should be valid");
    
    XCTAssertFalse([self.event isValidValue:@[]], @"an array should NOT be valid");
    
    XCTAssertFalse([self.event isValidValue:@{}], @"a dictionary should NOT be valid");
    
    XCTAssertFalse([self.event isValidValue:[NSNull class]], @"a NSNUll should NOT be valid");
}




- (void)testConvertToNumberBoolFromString
{
    NSNumber * shouldBeTrue = [self.event convertToNumberBoolFromString:@"true"];
    
    NSNumber * shouldBeFalse = [self.event convertToNumberBoolFromString:@"false"];
    
    NSNumber * shouldBeNil = [self.event convertToNumberBoolFromString:@"name"];

    XCTAssertTrue(shouldBeTrue.boolValue, @"this value should be YES");
    
    XCTAssertFalse(shouldBeFalse.boolValue, @"this value should be NO");
    
    XCTAssertNil(shouldBeNil, @"this value should be nil");
}



- (void)testParseDictionary
{
    NSDictionary * dictionary = [self dictionaryForJsonFileName:@"single"];
    
    for (NSAttributeDescription * attributeDescription in [self.event entityForClass].properties)
    {
        if ([attributeDescription.name isEqualToString:@"name"])
        {
            [self.event parseDictionary:dictionary forAttributeDescription:attributeDescription];
            
            XCTAssertTrue([self.event.name isEqualToString:@"name1"], @"the name should match that in the json %@", self.event.name);
        }
    }
}




- (void)testPopulatingNilDictionary
{
    NSError * error = nil;
    
    BOOL parsedOK = [self.event populateFromDictionary:nil error:&error]; // add test for error

    XCTAssertFalse(parsedOK, @"should not be able to parse with a nil dictionary");
    
    XCTAssertTrue([error.domain isEqualToString:ManageObjectParserDomainError], @"error domain incorrect");
    
    XCTAssertTrue(error.code == ManageObjectParserErrorInValidParameter, @"error code is not correct");
}



- (void)testPopulatingFromSingleFile
{
    NSDictionary * dictionary = [self dictionaryForJsonFileName:@"single"];
    
    BOOL parsedOK = [self.event populateFromDictionary:dictionary error:nil];
    
    XCTAssertTrue(parsedOK, @"populateFromDictionary should return YES, investigate");
    
    XCTAssertTrue([self.event.name isEqualToString:@"name1"], @"the name should match that in the json %@", self.event.name);
    
    XCTAssertFalse(self.event.flag.boolValue, @"the flag should match that in the json %d", self.event.flag.boolValue);
    
    XCTAssertTrue([self.event.value.stringValue isEqualToString:@"1.02"], @"attribute value is incorrect %@ %f", self.event.value.stringValue, self.event.value.floatValue);
}


- (void)testValidEntity
{
    self.event.name = @"name1";

    BOOL isValidEntity = [self.event isValidEntityWithError:nil]; // add test for error
    
    XCTAssertTrue(isValidEntity, @"the object should be valid as name has been set");
}



- (void)testValidEntityError
{
    NSError * error = nil;
    
    self.event.name = @"name1";
    
    [self.event isValidEntityWithError:&error];
    
    XCTAssertNil(error, @"the error should be nil for a valid entity");
}



- (void)testInValidEntity
{
    self.event.flag = @NO;
    
    BOOL isValidEntity = [self.event isValidEntityWithError:nil];
    
    XCTAssertFalse(isValidEntity, @"the object should be valid as name has been set");
}



- (void)testErrorForIncorrectParameterWithMessage
{
    NSError * error = [self.event errorForIncorrectParameterWithMessage:@"message"];
    
    XCTAssertTrue([error.domain isEqualToString:ManageObjectParserDomainError], @"error domain incorrect");
    
    XCTAssertTrue(error.code == ManageObjectParserErrorInValidParameter, @"error code is not correct");

    XCTAssertTrue([error.localizedDescription isEqualToString:@"message"], @"localized description incorrect");
}



- (void)testErrorForIncorrectParameterWithNilMessage
{
    NSError * error = [self.event errorForIncorrectParameterWithMessage:nil];
    
    XCTAssertTrue([error.domain isEqualToString:ManageObjectParserDomainError], @"error domain incorrect");
    
    XCTAssertTrue(error.code == ManageObjectParserErrorInValidParameter, @"error code is not correct");
}




- (void)testInValidEntityErorr
{
    NSError * error = nil;
    
    self.event.flag = @NO;
    
    [self.event isValidEntityWithError:&error];
    
    XCTAssertNotNil(error, @"the error should exist");
    
    XCTAssertTrue([error.domain isEqualToString:ManageObjectParserDomainError], @"error should have the corect domain");
    
    XCTAssertTrue(error.code == ManageObjectParseErrorNoneOptionalAttribute, @"error should have the corect code");
    
    NSString * attributeNameValue = error.userInfo[ManagedObjectErrorAttributeNameKey];
    
    XCTAssertTrue([attributeNameValue isEqualToString:@"name"], @"the error user information name key should be the attribute that is not optional and not set");
}




#pragma mark - testing end to end


- (void)testPopulatingFromSingleAltFile
{
    NSDictionary * dictionary = [self dictionaryForJsonFileName:@"singleAlt"];
    
    BOOL parsedOK = [self.event populateFromDictionary:dictionary error:nil];
    
    XCTAssertTrue(parsedOK, @"populateFromDictionary should return YES, investigate");
    
    XCTAssertTrue([self.event.name isEqualToString:@"name1"], @"the name should match that in the json %@", self.event.name);
    
    XCTAssertTrue(self.event.flag.boolValue, @"the flag should match that in the json %d", self.event.flag.boolValue);
}



- (void)testPopulatingEntityWithNonOptionalAttribute
{
    NSDictionary * dictionary = [self dictionaryForJsonFileName:@"singleNonOptional"];
    
    BOOL parsedOK = [self.event populateFromDictionary:dictionary error:nil];
    
    XCTAssertFalse(parsedOK, @"should fail as object is not valid");
}




@end
